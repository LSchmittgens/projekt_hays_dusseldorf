package com.binaris.linkfinder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkFilter {
	
	public List<String> filterSitesFromLinks(List<String> links) throws URISyntaxException {
		List<String> filteredSites = new ArrayList<String>();
		for(String link: links) {
			String domainName = getDomainName(link);
			if(!filteredSites.contains(domainName) && !domainName.trim().isEmpty()) {
				filteredSites.add(domainName);		
			}
		}
		return filteredSites;
	}
	
	public String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        if(domain != null) {
        	if (domain.startsWith("www.")) {
        		domain = domain.substring(4);
        	}
        	
        	int positionFirstPeriod = 0;
        	String shortenedDomain = domain;
        	for (int counter = 0; counter < domain.length(); counter++) {
        		if(domain.charAt(counter) == '.') {
        			if(positionFirstPeriod > 0) {
        				shortenedDomain = domain.substring(positionFirstPeriod + 1);
        			}
        			positionFirstPeriod = counter;
        		}
        	}

        	return shortenedDomain;
        }
        return "";
    }
}
