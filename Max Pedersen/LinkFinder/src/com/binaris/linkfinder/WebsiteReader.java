package com.binaris.linkfinder;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class WebsiteReader {
    public static List<String> readWebsite(String website) throws Exception {
    	String link = website;
    	if(!website.contains("www.")) {
    		link = "www." + website;
    	}
    	if(!(website.contains("http") || website.contains("https"))) {
    		link = "http://" + website;
    	}
    	Document doc = Jsoup.connect(link).get();
		Elements linkElements = doc.select("a[href]");
	   
	    List<String> links = new ArrayList<String>();
	    for (Element linkElement : linkElements) {
	    	links.add(linkElement.attr("abs:href"));
	    }
	    return links;
    }
}