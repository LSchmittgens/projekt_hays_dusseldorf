package com.binaris.linkfinder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class ReferencedWebsitesDetective {
	public static void main(String[] args) throws Exception {
		
		System.out.println("Please input a valid URL: ");
		
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String websiteLink = consoleReader.readLine();
        consoleReader.close();
		
		List<String> output;
		WebsiteReader urlReader = new WebsiteReader();
        output = urlReader.readWebsite(websiteLink);
        
        LinkFilter linkFilter = new LinkFilter();
        List<String> listOfReferencedSites = linkFilter.filterSitesFromLinks(output);
        
        System.out.printf("Number of referenced sites: " + listOfReferencedSites.size() + "\n");
        for(String domain: listOfReferencedSites ) {
        	System.out.printf(domain + "\n");
        }        
    }
}
