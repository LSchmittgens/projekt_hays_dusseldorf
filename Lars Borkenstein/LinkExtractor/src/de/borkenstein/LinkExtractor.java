package de.borkenstein;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LinkExtractor {
	private LinkExtractor() {}
	
	public static List<String>extractLinks(String url) throws IOException {
	    final ArrayList<String> result = new ArrayList<String>();

	    Document doc = Jsoup.connect(url).get();

	    Elements links = doc.select("a[href]");

	    for (Element link : links) {
	      result.add(link.attr("abs:href"));
	    }

	    return result;
	  }


	  public final static void main(String[] args) throws Exception{
	    String site = args[0];
	    ArrayList<String> domains = new ArrayList<String>();
	    List<String> links = LinkExtractor.extractLinks(site);
	    
	    for (String link : links) {
	      URI uri = new URI(link);
	      String domain = uri.getHost();
	      
	      if(domain.startsWith("www.")) {
	    	  domains.add(domain.substring(4));
	      } else {
	    	  domains.add(domain);
	      }
	    }
	    
	    Set<String> distinctEntries = new HashSet<>(domains);
	    for(String s : distinctEntries) {
	    	System.out.println(s + " - " + Collections.frequency(domains, s));
	    }
	  }

}
