package de.binaris.websitelinkanalyzer;

import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class WebsiteReaderTest {

    WebsiteReader websiteReader;

    @Before
    public void setup() {

    }

    @Test
    public void websiteReaderShouldCreateInputStreamFromValidUrl() throws IOException, IllegalArgumentException {
        String urlString = "http://www.google.de";
        websiteReader = new WebsiteReader(urlString);

        Elements result = websiteReader.getATagsFromUrl();

        Assert.assertNotNull("Result should be not null", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfUrlIsNotValid() throws IOException, IllegalArgumentException {
        String urlString = "www.google.de";
        websiteReader = new WebsiteReader(urlString);

        Elements result = websiteReader.getATagsFromUrl();

        Assert.assertNotNull("Result should be not null", result);
    }
}
