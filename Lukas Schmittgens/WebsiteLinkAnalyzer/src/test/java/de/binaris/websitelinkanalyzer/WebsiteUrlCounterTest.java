package de.binaris.websitelinkanalyzer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class WebsiteUrlCounterTest {

    WebsiteUrlCounter websiteUrlCounter;

    @Test
    public void domainsInWebsiteWillBeCounted() throws IOException {

        Element elementMock = Mockito.mock(Element.class);
        List<Element> elementCollection= new ArrayList<>();
        elementCollection.add(elementMock);
        elementCollection.add(elementMock);
        Elements elementsMock = new Elements(elementCollection);
        WebsiteReader websiteReaderMock = Mockito.mock(WebsiteReader.class);
        websiteUrlCounter = new WebsiteUrlCounter(websiteReaderMock);
        String elementsHrefLink = "http://testseite.de/news";

        Mockito.when(websiteReaderMock.getATagsFromUrl()).thenReturn(elementsMock);
        Mockito.when(elementMock.attr("href")).thenReturn(elementsHrefLink);

        Map<String, Integer> result = websiteUrlCounter.countLinksFromWebsite();

        String resultKey = "testseite.de";
        Assert.assertTrue(result.containsKey(resultKey));
        Assert.assertTrue(result.get(resultKey).equals(2));
    }
}
