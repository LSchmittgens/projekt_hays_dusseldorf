package de.binaris.websitelinkanalyzer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.net.URL;

public class WebsiteUrlCounter {

    private WebsiteReader websiteReader;

    WebsiteUrlCounter(WebsiteReader websiteReader) {
        this.websiteReader = websiteReader;
    }

    public Map<String, Integer> countLinksFromWebsite() throws IOException {
        Elements elements = websiteReader.getATagsFromUrl();
        Map<String, Integer> urlCountMap = new HashMap<>();
        for (Element element :  elements) {
            if(element.attr("href").contains("http"))
            this.validateElements(urlCountMap, element.attr("href"));
        }
        return urlCountMap;
    }

    private void validateElements(Map<String, Integer> linkMap, String hrefValue) throws IOException {
        URL url = new URL(hrefValue);
        String domain = url.getHost();
        if(linkMap.containsKey(domain)) {
            linkMap.put(domain, linkMap.get(domain)+1);
        } else {
            linkMap.put(domain, 1);
        }
    }
}
