package de.binaris.websitelinkanalyzer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.*;
import java.io.InputStreamReader;

public class WebsiteReader {

    private String urlString;

    WebsiteReader(String urlString) {
        this.urlString = urlString;
    }

    public Elements getATagsFromUrl() throws  IOException, IllegalArgumentException {

        Document doc = Jsoup.connect(urlString).get();
        return doc.select("a");

    }
}
