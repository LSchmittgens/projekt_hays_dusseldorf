package de.binaris.websitelinkanalyzer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("Please enter a URL with Format 'http://www....'");
            BufferedReader consoleReader =
                    new BufferedReader(new InputStreamReader(System.in));
            String url = consoleReader.readLine();
            consoleReader.close();
            WebsiteReader websiteReader = new WebsiteReader(url);
            WebsiteUrlCounter websiteUrlCounter = new WebsiteUrlCounter(websiteReader);
            Map<String, Integer> map = websiteUrlCounter.countLinksFromWebsite();
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " - " + entry.getValue());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
